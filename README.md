# Classification Sentences
---
## Run with docker
    > docker-compose up
Server api: http://127.0.0.1:8088/docs

## Run without docker
- Download [models](https://drive.google.com/file/d/1WQH9xyOUbeG-Y7qcHwlRqj5EYs4U5rZd/view?usp=sharing) and unzip models to app/models
- Download [Vietnamese Normalize](https://drive.google.com/file/d/1lDqeRlw0KYkluYIXDvGhR_OmZi5P4lZ9/view?usp=sharing) to app/scripts/data_loader

~~~
    > python3 --version
    Python 3.7.9
    > pip3 install -r requirements.txt
    > cd app
    > uvicorn main:app --port 8000 
~~~
Server api: http://127.0.0.1:8000/docs
