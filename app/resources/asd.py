import pandas as pd

corpus = pd.read_csv("./target_data_final.csv")
corpus["question"][3176:] = corpus["question"][3176:].apply(
    lambda x: x.replace("2", "")
)

corpus.to_csv("target_data_final.csv", index=False)

