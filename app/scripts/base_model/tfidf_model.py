from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.pipeline import Pipeline
from sklearn.metrics.pairwise import cosine_similarity as cs
from ..utils.constants import CLASSES_LARGE, CORPUS_TARGET, CLASSES_SMALL, CORPUS
import numpy as np


class TfidfModel:
    def __init__(self) -> None:
        self.target_sentence = np.array(CORPUS_TARGET["question"])
        self.labels = np.array(CORPUS_TARGET["labels"])
        self.title = np.array(CORPUS_TARGET["title"])
        self.model = Pipeline(
            [
                ("count_vector", CountVectorizer(ngram_range=(2, 3))),
                ("tfidf", TfidfTransformer(use_idf=True, norm="l2")),
            ]
        )

    def fit(self, corpus=CORPUS):
        self.model.fit(np.concatenate((corpus, self.target_sentence), axis=None))
        self.corpus_target = self.transform(self.target_sentence)

        dict_target = {}
        for index_title in CLASSES_LARGE:
            dict_target[index_title] = self.corpus_target[self.title == index_title]

        return dict_target

    def transform(self, corpus):
        return self.model.transform(corpus)

    def __get_max(self, n_probability, labels, threasold=0):
        new_p = []
        for i in n_probability:
            a = i.argsort()[-20:]
            result = [labels[index] for index in a if i[index] > threasold]
            if len(result) < 3:
                result = []
            new_p.append(result)
        return new_p

    def predict(self, corpus, target_embeddings, classes, labels, threasold=0):
        corpus_embedding = self.transform(corpus)
        result = cs(target_embeddings, corpus_embedding).T
        predicted = self.__get_max(result, labels, threasold)
        data = []
        labels_data = []
        for i in range(len(predicted)):
            if len(predicted[i]) > 0:
                data.append(corpus[i])
                labels_data.append(classes[max(predicted[i], key=predicted[i].count)])
            else:
                data.append(corpus[i])
                labels_data.append("nothing")

        return np.array(data), np.array(labels_data)

    def predict_large_class(self, corpus):
        return self.predict(corpus, self.corpus_target, CLASSES_LARGE, self.title)

    def __predict_small_class(self, corpus, target_embeddings, classes):
        return self.predict(
            corpus, target_embeddings, CLASSES_SMALL, classes, threasold=0.2
        )

    def predict_small_class(self, corpus):
        return self.predict(corpus, self.corpus_target, CLASSES_SMALL, self.labels)

    def predict_pipeline(self, corpus):
        data, labels_data = self.predict_large_class(corpus)
        result = {"corpus": [], "titles": [], "labels": []}

        for index_title in range(len(CLASSES_LARGE)):
            corpus = data[labels_data == CLASSES_LARGE[index_title]]
            try:
                target_embeddings = self.corpus_target[self.title == index_title]
                target_classes = self.labels[self.title == index_title]
                corpus_i, labels_i = self.__predict_small_class(
                    corpus, target_embeddings, target_classes
                )
                result["corpus"].extend(corpus_i)
                result["titles"].extend([CLASSES_LARGE[index_title]] * len(corpus_i))
                result["labels"].extend(labels_i)
            except:
                pass

        result["corpus"] = np.array(result["corpus"])
        result["titles"] = np.array(result["titles"])
        result["labels"] = np.array(result["labels"])

        return result
