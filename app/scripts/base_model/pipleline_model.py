from ..utils.constants import CLASSES_LARGE, CORPUS
import numpy as np
from sklearn.cluster import AgglomerativeClustering
from .tfidf_model import TfidfModel
import scipy.sparse as sp


class Model:
    def __init__(self, corpus=CORPUS, num_clusters=2) -> None:
        self.num_clusters = num_clusters

        self.model_tfidf = TfidfModel()
        self.dict_target_emb = self.model_tfidf.fit(corpus)

    def predict(self, corpus):
        tfidf_result = self.model_tfidf.predict_pipeline(corpus)

        corpus_rest = corpus.copy()
        for i in tfidf_result["corpus"]:
            if i in corpus:
                corpus_rest.remove(i)

        for index_title in CLASSES_LARGE:
            index_nothing = np.logical_and(
                tfidf_result["labels"] == "nothing",
                tfidf_result["titles"] == index_title,
            )
            if True not in index_nothing:
                continue

            model_clustering = AgglomerativeClustering()
            corpus = tfidf_result["corpus"][index_nothing]
            corpus_embeddings = self.model_tfidf.transform(corpus)

            model_clustering.fit(
                sp.vstack(
                    (corpus_embeddings, self.dict_target_emb[index_title]), format="csr"
                ).toarray()
            )
            tfidf_result["labels"][index_nothing] = model_clustering.labels_[
                : corpus_embeddings.shape[0]
            ]

        tfidf_result["corpus_rest"] = np.array(corpus_rest)

        for i in tfidf_result.keys():
            tfidf_result[i] = tfidf_result[i].tolist()

        return tfidf_result
