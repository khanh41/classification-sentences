import re
import string
from . import VietnameseTextNormalizer
from ..utils.constants import STOP_WORDS, TEENCODE
import emoji

translator = str.maketrans(string.punctuation, " " * len(string.punctuation))


class Preprocessing:
    def __init__(self) -> None:
        pass

    def __loaddicchar(self):
        dic = {}
        char1252 = "à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ".split(
            "|"
        )
        charutf8 = "à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ".split(
            "|"
        )
        for i in range(len(char1252)):
            dic[char1252[i]] = charutf8[i]
        return dic

    def __covert_unicode(self, txt):
        dicchar = self.__loaddicchar()
        return re.sub(
            r"à|á|ả|ã|ạ|ầ|ấ|ẩ|ẫ|ậ|ằ|ắ|ẳ|ẵ|ặ|è|é|ẻ|ẽ|ẹ|ề|ế|ể|ễ|ệ|ì|í|ỉ|ĩ|ị|ò|ó|ỏ|õ|ọ|ồ|ố|ổ|ỗ|ộ|ờ|ớ|ở|ỡ|ợ|ù|ú|ủ|ũ|ụ|ừ|ứ|ử|ữ|ự|ỳ|ý|ỷ|ỹ|ỵ|À|Á|Ả|Ã|Ạ|Ầ|Ấ|Ẩ|Ẫ|Ậ|Ằ|Ắ|Ẳ|Ẵ|Ặ|È|É|Ẻ|Ẽ|Ẹ|Ề|Ế|Ể|Ễ|Ệ|Ì|Í|Ỉ|Ĩ|Ị|Ò|Ó|Ỏ|Õ|Ọ|Ồ|Ố|Ổ|Ỗ|Ộ|Ờ|Ớ|Ở|Ỡ|Ợ|Ù|Ú|Ủ|Ũ|Ụ|Ừ|Ứ|Ử|Ữ|Ự|Ỳ|Ý|Ỷ|Ỹ|Ỵ",
            lambda x: dicchar[x.group()],
            txt,
        )

    def __replace_teencode(self, data_question):
        data_question += " "
        for j in range(len(TEENCODE)):
            while True:
                if data_question.find(" " + TEENCODE["teencode"][j] + " ") != -1:
                    index = data_question.find(" " + TEENCODE["teencode"][j] + " ")
                    data_question = (
                        data_question[: index + 1]
                        + TEENCODE["non teencode"][j]
                        + data_question[index + len(" " + TEENCODE["teencode"][j]) :]
                    )

                elif data_question.find(TEENCODE["teencode"][j] + " ") == 0:
                    index = data_question.find(TEENCODE["teencode"][j] + " ")
                    data_question = (
                        TEENCODE["non teencode"][j]
                        + data_question[index + len(TEENCODE["teencode"][j]) :]
                    )

                elif data_question.find(
                    " " + TEENCODE["teencode"][j]
                ) != -1 and data_question.find(" " + TEENCODE["teencode"][j]) == len(
                    data_question
                ) - len(
                    TEENCODE["teencode"][j] + " "
                ):
                    index = data_question.find(" " + TEENCODE["teencode"][j])
                    data_question = data_question[:index] + TEENCODE["non teencode"][j]
                else:
                    break
        return data_question

    def preprocessing(self, data):
        data = [str(x) for x in data if len(str(x)) > 5 and len(str(x)) < 200]
        for i in range(len(data)):
            try:
                data[i] = data[i].lower()
                data[i] = self.__covert_unicode(data[i])
                data[i] = re.sub(r"\d+", "", data[i])
                data[i] = data[i].replace("\r", "")
                data[i] = data[i].replace("\n", " ")
                data[i] = data[i].replace("\t", " ")
                data[i] = data[i].translate(translator)
                data[i] = emoji.demojize(data[i])
                data[i] = self.__replace_teencode(data[i])

                data[i] = VietnameseTextNormalizer.Normalize(
                    " UTF8 : " + data[i]
                ).replace(" UTF8 : ", "")

                # for stopword in STOP_WORDS:
                #     if " " + stopword.lower() + " " in data[i]:
                #         data[i] = data[i].replace(" " + stopword.lower() + " ", " ")

                data[i] = re.sub(r'[" "]+', " ", data[i])
                data[i] = data[i].strip()
            except:
                print("error: " + data[i])

        data = [str(x) for x in data if len(str(x)) > 5 and len(str(x)) < 200]

        return data
