from .rake import Rake


class GetTopToppic:
    def __init__(self) -> None:
        self.rake_object = Rake("./resources/stopword_topic.txt", 5, 5, 1)

    def extraction(self, list_sentences):
        temp = ""

        for i in list_sentences:
            temp = temp + " . " + str(i)

        keywords = self.rake_object.run(temp)

        dict_words = {}

        for i in range(len(keywords)):
            target = list(keywords[i])
            target[1] = 0
            for text in list_sentences:
                if target[0] in text:
                    target[1] += 1

            dict_words[target[0]] = target[1]
        return dict(sorted(dict_words.items(), key=lambda item: item[1], reverse=True))

