import pandas as pd

CLASSES_SMALL = [
    "nothing",
    "Người yêu mình không còn yêu mình nữa",
    "Người mình thích không thích mình",
    "Không ai yêu",
    "Người yêu bỏ",
    "Hỏi chuyện về crush",
    "Hỏi về người yêu",
    "Chia tay người yêu",
    "Người yêu tệ",
    "Muốn có người yêu",
    "Bị cắm sừng",
    "Yêu bạn thân",
    "Bị la mắng",
    "Buồn",
    "Áp lực",
    "Xem phim",
    "Bị ba mẹ đánh",
    "Cảm thấy mệt mỏi",
    "Vui vẻ",
    "Một mình",
    "Tặng quà",
    "Chơi game",
    "Không thể tập trung học hành",
    "Bị điểm kém",
    "làm bài tập về nhà",
    "Học thêm",
    "Sổ đầu bài",
    "Đậu đại học",
    "Không ai chơi",
    "Bạn thân",
]

CLASSES_LARGE = ["Tình yêu", "Chuyện thường ngày", "Học hành", "Bạn bè"]

CORPUS_TARGET = pd.read_csv("./resources/target_data_final.csv", error_bad_lines=False)

CORPUS = open("./resources/datasumi.txt", "r").read().split("\n")

STOP_WORDS = open("./resources/stopword.txt").read()
STOP_WORDS = STOP_WORDS.split("\n")

TEENCODE = pd.read_csv("./resources/teencode.csv")
