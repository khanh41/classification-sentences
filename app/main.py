# 1. Library imports
from scripts.top_topic import GetTopToppic
from fastapi import FastAPI, File, UploadFile, Query
from prediction import read_image, predict, detect_face, generate_caption
import numpy as np
from typing import List, Optional
from scripts.base_model.pipleline_model import Model
from scripts.data_loader.preprocessing_vi import Preprocessing

app = FastAPI()

model = Model()

extraction_topic = GetTopToppic()


@app.get("/")
async def root():
    return "go /docs"


@app.post("/predict-classes/", summary="To predict class of list corpus")
async def predict_classes(corpus: Optional[List[str]] = Query(None)):
    """
    Predict class for list corpus
    - **corpus**: list sentences

    **Return**:
    - **corpus**: list sentences
    - **titles**: titles corresponding, ex: Tình yêu, Chuyện thường ngày, Học hành, Bạn bè,...
    - **labels**: labels corresponding, ex: Người mình thích không thích mình, Không ai yêu,...
    """
    corpus = Preprocessing().preprocessing(corpus)
    return model.predict(corpus)


@app.post("/predict-classes-csv/", summary="To predict class with csv file")
def upload_csv(csvfile: UploadFile = File(...)):
    extension = csvfile.filename.split(".")[-1]
    if not extension in ("csv", "txt"):
        return "File should be txt or csv type"

    corpus = csvfile.file.read()
    corpus = corpus.decode("utf-8")
    corpus = corpus.split("\n")
    corpus = Preprocessing().preprocessing(corpus)
    return model.predict(corpus)


@app.post("/reload-sever", summary="Đừng đụng vô, don't touch it")
async def reload_server():
    corpus = open("./resources/datasumi.txt", "r").read().split("\n")
    corpus = Preprocessing().preprocessing(corpus)
    with open("./resources/datasumi.txt", "w") as f:
        for item in corpus:
            f.write("%s\n" % item)

    global model
    model = Model(corpus)

    return {"message": "Success"}


@app.post("/predict")
async def predict_api(file: UploadFile = File(...)):
    """
    Predict gender of people in picture.
    """
    extension = file.filename.split(".")[-1] in ("jpg", "jpeg", "png")
    if not extension:
        return "Image must be jpg or png format!"

    image = read_image(await file.read())

    try:
        face, _ = detect_face(image)
        normalize_face = face
        normalize_face = np.array(face * 255.0, dtype=np.int)
        gender, emotion, cap = predict(normalize_face)
        return gender, emotion, cap
    except Exception as e:
        print(e)

    gender = "MEN"
    emotion = "SMILE"
    cap = generate_caption(gender, emotion)
    return gender, emotion, cap


@app.post("/extraction-topic/", summary="To extraction topic with csv file")
def extract_topic_by_file(csvfile: UploadFile = File(...)):
    extension = csvfile.filename.split(".")[-1]
    if not extension in ("csv", "txt"):
        return "File should be txt or csv type"

    corpus = csvfile.file.read()
    corpus = corpus.decode("utf-8")
    corpus = corpus.split("\n")
    return extraction_topic.extraction(corpus)


if __name__ == "__main__":
    import uvicorn

    uvicorn.run(app, host="0.0.0.0", port=8088)
